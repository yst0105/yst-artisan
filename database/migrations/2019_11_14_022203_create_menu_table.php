<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tb_menus')) {
            Schema::create('tb_menus', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 20)->comment('選單名稱');
                $table->string('code', 20)->comment('選單代碼');
                $table->tinyInteger('idParent')->default(0)->comment('上層選單');
                $table->string('icon', 50)->comment('選單icon');
                $table->string('route', 30)->comment('路由');
                $table->integer('sort')->default(1)->comment('排序');
                $table->tinyInteger('isEnabled')->index()->default(1)->comment('啟用狀態');
                $table->integer('createdBy')->default(0)->comment('建立者');
                $table->integer('updatedBy')->default(0)->comment('修改者');
                $table->dateTime('createdOn')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('建立日期');
                $table->dateTime('updatedOn')->index()->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'))->comment('最後更新時間');
                $table->tinyInteger('isDeleted')->default(0)->index()->comment('軟刪除 (0:未刪除|1:已刪除)');
                // $table->timestamps();

                // 索引
                $table->unique('code');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_menus');
    }
}
