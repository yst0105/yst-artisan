<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'AuthController@login');


Route::get('/roles', 'RoleController@getRoleList');
Route::post('/roles', 'RoleController@createRole');
Route::get('/roles/{id}', 'RoleController@getRole');
Route::patch('/roles/{id}', 'RoleController@updateRole');
Route::delete('/roles/{id}', 'RoleController@deleteRole');


Route::get('/userRoles', 'UserRoleController@getUserRoleList');
Route::post('/userRoles', 'UserRoleController@createUserRole');
Route::get('/userRoles/{id}', 'UserRoleController@getUserRole');
Route::patch('/userRoles/{id}', 'UserRoleController@updateUserRole');
Route::delete('/userRoles/{id}', 'UserRoleController@deleteUserRole');

//Route::get('/sideMenus', 'MenuController@getSideMenu');
Route::get('/menus', 'MenuController@getMenuList');
Route::get('/menuCatalogs', 'MenuController@getMenuCatalogOption');
Route::post('/menus', 'MenuController@createMenu');
Route::get('/menus/{id}', 'MenuController@getMenu');
Route::patch('/menus/{id}', 'MenuController@updateMenu');
Route::delete('/menus/{id}', 'MenuController@deleteMenu');

Route::get('/sideMenus', 'PermissionController@getSidePermission');
Route::get('/permissions/{id}', 'PermissionController@getPermissionList');
