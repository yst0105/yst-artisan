/*
MySQL Backup
Database: yst
Backup Time: 2019-11-15 16:51:12
*/

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `yst`.`menus`;
DROP TABLE IF EXISTS `yst`.`tb_menus_copy1`;
DROP TABLE IF EXISTS `yst`.`tb_permissions`;
DROP TABLE IF EXISTS `yst`.`tb_role_permissions`;
DROP TABLE IF EXISTS `yst`.`tb_role_users`;
DROP TABLE IF EXISTS `yst`.`tb_roles`;
DROP TABLE IF EXISTS `yst`.`tb_users`;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '選單名稱',
  `code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '選單代碼',
  `idParent` tinyint(4) NOT NULL DEFAULT '0' COMMENT '上層選單',
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '選單icon',
  `route` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '路由',
  `sort` int(11) NOT NULL DEFAULT '1' COMMENT '排序',
  `isEnabled` tinyint(4) NOT NULL DEFAULT '1' COMMENT '啟用狀態',
  `createdBy` int(11) NOT NULL DEFAULT '0' COMMENT '建立者',
  `updatedBy` int(11) NOT NULL DEFAULT '0' COMMENT '修改者',
  `createdOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立日期',
  `updatedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最後更新時間',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '軟刪除 (0:未刪除|1:已刪除)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_code_unique` (`code`),
  KEY `menus_isenabled_index` (`isEnabled`),
  KEY `menus_updatedon_index` (`updatedOn`),
  KEY `menus_isdeleted_index` (`isDeleted`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `tb_menus_copy1` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '選單名稱',
  `code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '選單代碼',
  `idParent` tinyint(4) NOT NULL DEFAULT '0' COMMENT '上層選單',
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '選單icon',
  `route` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '路由',
  `sort` int(11) NOT NULL DEFAULT '1' COMMENT '排序',
  `isEnabled` tinyint(4) NOT NULL DEFAULT '1' COMMENT '啟用狀態',
  `createdBy` int(11) NOT NULL DEFAULT '0' COMMENT '建立者',
  `updatedBy` int(11) NOT NULL DEFAULT '0' COMMENT '修改者',
  `createdOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立日期',
  `updatedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最後更新時間',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '軟刪除 (0:未刪除|1:已刪除)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tb_menus_code_unique` (`code`),
  KEY `tb_menus_isenabled_index` (`isEnabled`),
  KEY `tb_menus_updatedon_index` (`updatedOn`),
  KEY `tb_menus_isdeleted_index` (`isDeleted`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `tb_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '權限代碼',
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '權限',
  `isEnabled` tinyint(4) NOT NULL DEFAULT '1' COMMENT '啟用狀態',
  `createdBy` int(11) NOT NULL DEFAULT '0' COMMENT '建立者',
  `updatedBy` int(11) NOT NULL DEFAULT '0' COMMENT '修改者',
  `createdOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立日期',
  `updatedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最後更新時間',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '軟刪除 (0:未刪除|1:已刪除)',
  PRIMARY KEY (`id`),
  KEY `tb_permissions_isenabled_index` (`isEnabled`),
  KEY `tb_permissions_updatedon_index` (`updatedOn`),
  KEY `tb_permissions_isdeleted_index` (`isDeleted`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `tb_role_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idRoles` int(11) NOT NULL DEFAULT '0' COMMENT '角色ID',
  `permissions` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '權限',
  `isEnabled` tinyint(4) NOT NULL DEFAULT '1' COMMENT '啟用狀態',
  `createdBy` int(11) NOT NULL DEFAULT '0' COMMENT '建立者',
  `updatedBy` int(11) NOT NULL DEFAULT '0' COMMENT '修改者',
  `createdOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立日期',
  `updatedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最後更新時間',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '軟刪除 (0:未刪除|1:已刪除)',
  PRIMARY KEY (`id`),
  KEY `tb_role_permissions_isenabled_index` (`isEnabled`),
  KEY `tb_role_permissions_updatedon_index` (`updatedOn`),
  KEY `tb_role_permissions_isdeleted_index` (`isDeleted`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `tb_role_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idUsers` int(11) NOT NULL DEFAULT '0' COMMENT '使用者ID',
  `idRoles` int(11) NOT NULL DEFAULT '0' COMMENT '角色ID',
  `isEnabled` tinyint(4) NOT NULL DEFAULT '1' COMMENT '啟用狀態',
  `createdBy` int(11) NOT NULL DEFAULT '0' COMMENT '建立者',
  `updatedBy` int(11) NOT NULL DEFAULT '0' COMMENT '修改者',
  `createdOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立日期',
  `updatedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最後更新時間',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '軟刪除 (0:未刪除|1:已刪除)',
  PRIMARY KEY (`id`),
  KEY `tb_role_users_isenabled_index` (`isEnabled`),
  KEY `tb_role_users_updatedon_index` (`updatedOn`),
  KEY `tb_role_users_isdeleted_index` (`isDeleted`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `tb_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '角色名稱',
  `remark` text COLLATE utf8mb4_unicode_ci COMMENT '備註',
  `isEnabled` tinyint(4) NOT NULL DEFAULT '1' COMMENT '啟用狀態',
  `createdBy` int(11) NOT NULL DEFAULT '0' COMMENT '建立者',
  `updatedBy` int(11) NOT NULL DEFAULT '0' COMMENT '修改者',
  `createdOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立日期',
  `updatedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最後更新時間',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '軟刪除 (0:未刪除|1:已刪除)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `name` (`name`),
  KEY `isEnabled` (`isEnabled`),
  KEY `isDeleted` (`isDeleted`) USING BTREE,
  KEY `createdBy` (`createdBy`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE `tb_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '使用者帳號',
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci COMMENT 'JWT Token',
  `isEnabled` tinyint(4) NOT NULL DEFAULT '1' COMMENT '啟用狀態',
  `createdBy` int(11) NOT NULL DEFAULT '0' COMMENT '建立者',
  `updatedBy` int(11) NOT NULL DEFAULT '0' COMMENT '修改者',
  `createdOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立日期',
  `updatedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最後更新時間',
  `isDeleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '軟刪除 (0:未刪除|1:已刪除)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account` (`account`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
BEGIN;
LOCK TABLES `yst`.`menus` WRITE;
DELETE FROM `yst`.`menus`;
INSERT INTO `yst`.`menus` (`id`,`name`,`code`,`idParent`,`icon`,`route`,`sort`,`isEnabled`,`createdBy`,`updatedBy`,`createdOn`,`updatedOn`,`isDeleted`) VALUES (1, '帳號管理', 'accounts', 0, 'el-icon-setting', 'accounts', 1, 1, 0, 0, '2019-11-14 11:31:57', '2019-11-14 11:31:57', 0),(2, '選單管理', 'menus', 0, 'el-icon-menu', 'menus', 1, 1, 0, 0, '2019-11-14 11:31:57', '2019-11-14 11:31:57', 0),(3, '權限管理', 'permissions', 0, 'el-icon-sold-out', 'permissions', 1, 1, 0, 0, '2019-11-14 11:31:57', '2019-11-14 11:31:57', 0),(4, '通知管理', 'notifies', 0, 'el-icon-bell', 'notifies', 1, 1, 0, 0, '2019-11-14 11:31:57', '2019-11-14 11:31:57', 0),(5, '庫存管理', 'materials', 0, 'el-icon-document', 'materials', 1, 1, 0, 0, '2019-11-14 11:31:57', '2019-11-14 11:31:57', 0),(6, 'Telegram', 'telegram', 4, '', 'telegram', 1, 1, 0, 0, '2019-11-14 11:31:57', '2019-11-14 11:31:57', 0),(7, 'Line', 'line', 4, '', 'line', 1, 1, 0, 0, '2019-11-14 11:31:57', '2019-11-14 11:31:57', 0),(8, '角色列表', 'roles', 1, '', 'roles', 1, 1, 0, 0, '2019-11-14 11:31:57', '2019-11-14 11:31:57', 0),(9, '使用者列表', 'users', 1, '', 'users', 1, 1, 0, 0, '2019-11-14 11:31:57', '2019-11-14 11:31:57', 0);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `yst`.`tb_menus_copy1` WRITE;
DELETE FROM `yst`.`tb_menus_copy1`;
INSERT INTO `yst`.`tb_menus_copy1` (`id`,`name`,`code`,`idParent`,`icon`,`route`,`sort`,`isEnabled`,`createdBy`,`updatedBy`,`createdOn`,`updatedOn`,`isDeleted`) VALUES (1, '帳號管理', 'accounts', 0, 'el-icon-setting', 'accounts', 1, 1, 0, 0, '2019-08-08 08:49:14', '2019-08-08 08:49:14', 0),(2, '選單管理', 'menus', 0, 'el-icon-menu', 'menus', 1, 1, 0, 0, '2019-08-08 08:49:14', '2019-08-08 08:49:14', 0),(3, '權限管理', 'permissions', 0, 'el-icon-sold-out', 'permissions', 1, 1, 0, 0, '2019-08-08 08:49:14', '2019-08-08 08:49:14', 0),(4, '通知管理', 'notifies', 0, 'el-icon-bell', 'notifies', 1, 1, 0, 0, '2019-08-08 08:49:14', '2019-08-08 08:49:14', 0),(5, '庫存管理', 'materials', 0, 'el-icon-document', 'materials', 1, 1, 0, 0, '2019-08-08 08:49:14', '2019-08-08 08:49:14', 0),(6, 'Telegram Bot', 'telegramBot', 4, '', 'telegramBot', 1, 1, 0, 0, '2019-08-08 08:49:14', '2019-08-08 08:49:14', 0),(7, 'Line Bot', 'lineBot', 4, '', 'lineBot', 1, 1, 0, 0, '2019-08-08 08:49:14', '2019-08-08 08:49:14', 0),(8, '角色列表', 'roles', 1, '', 'roles', 1, 1, 0, 0, '2019-08-08 08:49:14', '2019-08-08 08:49:14', 0),(9, '使用者列表', 'users', 1, '', 'users', 1, 1, 0, 0, '2019-08-08 08:49:14', '2019-08-29 08:26:10', 0);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `yst`.`tb_permissions` WRITE;
DELETE FROM `yst`.`tb_permissions`;
INSERT INTO `yst`.`tb_permissions` (`id`,`code`,`name`,`isEnabled`,`createdBy`,`updatedBy`,`createdOn`,`updatedOn`,`isDeleted`) VALUES (1, 'create', '新增', 1, 0, 0, '2019-08-08 08:49:14', '2019-08-08 08:49:14', 0),(2, 'update', '修改', 1, 0, 0, '2019-08-08 08:49:14', '2019-08-08 08:49:14', 0),(3, 'delete', '刪除', 1, 0, 0, '2019-08-08 08:49:14', '2019-08-08 08:49:14', 0),(4, 'view', '檢視', 1, 0, 0, '2019-08-08 08:49:14', '2019-08-08 08:49:14', 0);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `yst`.`tb_role_permissions` WRITE;
DELETE FROM `yst`.`tb_role_permissions`;
INSERT INTO `yst`.`tb_role_permissions` (`id`,`idRoles`,`permissions`,`isEnabled`,`createdBy`,`updatedBy`,`createdOn`,`updatedOn`,`isDeleted`) VALUES (1, 1, '{\"accounts\":{\"create\":true,\"update\":true,\"delete\":true,\"view\":true},\"menus\":{\"create\":true,\"update\":true,\"delete\":true,\"view\":true},\"permissions\":{\"create\":true,\"update\":true,\"delete\":true,\"view\":true},\"notifies\":{\"create\":true,\"update\":true,\"delete\":true,\"view\":true},\"materials\":{\"create\":true,\"update\":true,\"delete\":true,\"view\":true},\"telegramBot\":{\"create\":true,\"update\":true,\"delete\":true,\"view\":true},\"lineBot\":{\"create\":true,\"update\":true,\"delete\":true,\"view\":true},\"roles\":{\"create\":true,\"update\":true,\"delete\":true,\"view\":true},\"users\":{\"create\":true,\"update\":true,\"delete\":true,\"view\":true}}', 1, 0, 0, '2019-08-08 08:49:14', '2019-08-08 08:49:14', 0),(2, 2, '{\"accounts\":{\"create\":true,\"update\":true,\"delete\":true,\"view\":true},\"menus\":{\"create\":true,\"update\":true,\"delete\":true,\"view\":true},\"roles\":{\"create\":true,\"update\":true,\"delete\":true,\"view\":true},\"users\":{\"create\":true,\"update\":true,\"delete\":true,\"view\":true}}', 1, 0, 0, '2019-08-08 08:49:14', '2019-08-08 08:49:14', 0),(3, 3, '{\"notifies\":{\"create\":true,\"update\":false,\"delete\":true,\"view\":true},\"telegramBot\":{\"create\":true,\"update\":true,\"delete\":false,\"view\":true},\"lineBot\":{\"create\":false,\"update\":false,\"delete\":false,\"view\":true}}', 1, 0, 0, '2019-08-08 08:49:14', '2019-08-20 02:18:21', 0),(4, 4, '{\"materials\":{\"create\":true,\"update\":false,\"delete\":true,\"view\":true}}', 1, 0, 0, '2019-08-08 08:49:14', '2019-08-15 06:16:05', 0),(5, 0, '{\"notifies\":{\"create\":true,\"update\":false,\"delete\":true,\"view\":true},\"telegramBot\":{\"create\":true,\"update\":true,\"delete\":false,\"view\":true},\"lineBot\":{\"create\":false,\"update\":false,\"delete\":false,\"view\":true}}', 1, 0, 0, '2019-08-20 02:13:37', '2019-08-20 02:13:37', 0);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `yst`.`tb_role_users` WRITE;
DELETE FROM `yst`.`tb_role_users`;
INSERT INTO `yst`.`tb_role_users` (`id`,`idUsers`,`idRoles`,`isEnabled`,`createdBy`,`updatedBy`,`createdOn`,`updatedOn`,`isDeleted`) VALUES (1, 1, 1, 1, 1, 0, '2019-08-14 08:49:14', '2019-08-12 08:24:47', 0),(2, 2, 2, 1, 1, 0, '2019-08-08 08:49:14', '2019-08-12 06:19:31', 0),(3, 2, 3, 1, 1, 0, '2019-08-16 08:49:14', '2019-08-12 06:19:31', 0),(4, 3, 2, 1, 1, 1, '2019-08-06 08:49:14', '2019-08-20 02:28:59', 0);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `yst`.`tb_roles` WRITE;
DELETE FROM `yst`.`tb_roles`;
INSERT INTO `yst`.`tb_roles` (`id`,`name`,`remark`,`isEnabled`,`createdBy`,`updatedBy`,`createdOn`,`updatedOn`,`isDeleted`) VALUES (1, '最高管理員', '．長短期策略規劃、政策推動、規章制訂及轉投資事業督導\r\n．綜理公司整體事業之決策、執行與考核\r\n．組織全球所有部門及分公司進行協調與運作，以達成營運目標', 1, 1, 0, '2019-08-15 08:49:14', '2019-08-21 05:08:45', 0),(2, '研發工程師', '．研發後產品可行性測試\r\n．產品相容性測試\r\n．支援客戶服務部、資訊管理部、行銷企劃部等', 1, 1, 0, '2019-08-16 08:49:14', '2019-08-21 05:09:07', 0),(3, '通知管理員', '．負責將公司產品之訊息發佈給相關媒體\r\n．一般平面及電子媒體之聯絡窗口\r\n．規劃公司協助公益團體舉辦之各種活動', 1, 1, 0, '2019-08-24 08:49:14', '2019-08-21 05:09:00', 0),(4, '工讀生', '工讀生做雜事', 1, 1, 0, '2019-08-08 08:49:14', '2019-08-21 05:25:29', 0),(5, '執行長', '．公司資金管理、規劃與執行\r\n．公司會計事務處理及編製管理報表供決策分析\r\n．租稅減免等稅務相關業務', 1, 1, 0, '2019-08-21 04:01:21', '2019-08-21 05:25:17', 0),(6, '資訊長', '．產品行銷企劃及行銷通路整合\r\n．建置公司網際網路之網站，負責在網路上介紹公司及產品行銷\r\n．負責產品行銷之廣告、產品型錄之發行，支援業務部之行銷活動\r\n．產品推廣與對媒體聯繫，編輯及發行公司刊物\r\n．國內外電腦秀展之規劃及執行\r', 1, 1, 0, '2019-08-21 04:01:32', '2019-08-21 05:25:17', 0),(7, '特別助理', '．亞洲業務之產品銷售\r\n．研擬亞洲市場行銷策略及產品訂價\r\n．協調交貨、收款\r\n．開發亞洲業務、定期拜訪客戶，發掘客戶問題與處理', 1, 1, 0, '2019-08-21 04:01:40', '2019-08-29 08:34:09', 0),(8, '協理', '．業務之專案管理\r\n．事業群業務之支援\r\n．技術問題跟催與解決', 1, 1, 0, '2019-08-21 04:01:46', '2019-08-29 08:34:09', 0),(9, '資訊部組長', '評估與追蹤查核公司內部控制制度及各項管理制度之健全性、合理性、有效性', 1, 1, 0, '2019-08-21 04:01:56', '2019-08-29 08:34:09', 0),(10, '資訊部副理', NULL, 0, 1, 0, '2019-08-21 04:02:09', '2019-08-22 08:54:53', 1),(11, '管理顧問', NULL, 0, 1, 0, '2019-08-21 04:02:23', '2019-08-22 08:54:48', 1),(12, ' 行政經理', '．蒐集與分析日本地區市場資訊\r\n．負責日本當地市場之產品行銷及業務推廣\r\n．負責日本當地市場之產品售後服務與客戶技術支援', 1, 1, 0, '2019-08-21 04:02:38', '2019-11-13 13:16:25', 0),(13, '採購員', '亂買一通', 1, 1, 0, '2019-08-21 04:03:11', '2019-08-22 08:52:30', 1),(14, '測試角色', NULL, 1, 1, 0, '2019-08-21 09:27:18', '2019-08-22 08:30:49', 1),(15, 'RD', NULL, 1, 1, 0, '2019-08-22 07:25:04', '2019-08-22 08:30:45', 1),(16, '測試', NULL, 1, 1, 0, '2019-08-22 07:28:09', '2019-08-22 09:12:15', 1),(17, '測試123', NULL, 1, 1, 0, '2019-08-22 07:32:48', '2019-08-22 09:12:08', 1),(18, '123', NULL, 0, 1, 0, '2019-08-22 07:40:32', '2019-08-22 09:12:03', 1),(19, '1', NULL, 0, 1, 0, '2019-08-22 07:44:52', '2019-08-22 09:03:43', 1),(20, '3221', NULL, 0, 1, 0, '2019-08-22 07:45:40', '2019-08-22 09:04:21', 1),(21, '3', NULL, 0, 1, 0, '2019-08-22 07:47:16', '2019-08-22 09:03:54', 1),(22, '22', NULL, 0, 1, 0, '2019-08-22 08:12:28', '2019-08-22 09:03:49', 1),(23, '測試2', NULL, 1, 1, 0, '2019-08-22 08:59:13', '2019-08-22 09:02:47', 1),(24, '測試測試111', NULL, 1, 1, 0, '2019-08-22 09:00:09', '2019-08-22 09:01:31', 1),(25, '2', NULL, 0, 1, 0, '2019-08-22 09:04:29', '2019-08-22 09:06:33', 1),(26, '2123', NULL, 0, 1, 0, '2019-08-22 09:04:38', '2019-08-22 09:04:57', 1),(27, '21231', NULL, 0, 1, 0, '2019-08-22 09:05:13', '2019-08-22 09:05:25', 1),(28, '123321', NULL, 0, 1, 0, '2019-08-22 09:06:41', '2019-08-22 09:11:58', 1),(29, '1233212', NULL, 0, 1, 0, '2019-08-22 09:06:47', '2019-08-22 09:08:24', 1),(30, '123321222', NULL, 0, 1, 0, '2019-08-22 09:07:14', '2019-08-22 09:08:19', 1),(31, '222', NULL, 0, 1, 0, '2019-08-22 09:08:07', '2019-08-22 09:08:14', 1),(32, '221321313', NULL, 0, 1, 0, '2019-08-22 09:09:05', '2019-08-22 09:09:38', 1),(33, '21111', NULL, 0, 1, 0, '2019-08-22 09:09:24', '2019-08-22 09:09:31', 1),(34, '12123123213', NULL, 0, 1, 0, '2019-08-22 09:14:38', '2019-08-22 09:14:38', 0),(35, '', NULL, 1, 0, 0, '2019-11-13 13:16:27', '2019-11-13 13:16:27', 0);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `yst`.`tb_users` WRITE;
DELETE FROM `yst`.`tb_users`;
INSERT INTO `yst`.`tb_users` (`id`,`name`,`account`,`password`,`token`,`isEnabled`,`createdBy`,`updatedBy`,`createdOn`,`updatedOn`,`isDeleted`) VALUES (1, 'Tinn', 'y01', '$2y$10$4W/9rxAoX2DqJ5Avjpvlre9q2oIi.yp6fFu1R.9LpS2.gPC/v1Ih6', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODAwMFwvYXBpXC9sb2dpbiIsImlhdCI6MTU3MzgwNzU4OSwiZXhwIjoxNTczODExMTg5LCJuYmYiOjE1NzM4MDc1ODksImp0aSI6IldZNkNueXk1Y0RhTXVmTEEiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEiLCJpZCI6MSwibmFtZSI6IlRpbm4iLCJhY2NvdW50IjoieTAxIn0.7QYftQf9f3STcm6Xyn1XYtmybHWq6HrQlc-2oca2Ze0', 1, 1, 0, '2019-08-08 08:49:14', '2019-11-15 16:46:29', 0),(2, 'Ming', 'y02', '$2y$10$4W/9rxAoX2DqJ5Avjpvlre9q2oIi.yp6fFu1R.9LpS2.gPC/v1Ih6', NULL, 1, 1, 0, '2019-08-08 08:49:14', '2019-11-15 15:13:44', 0),(3, 'Ben', 'y03', '$2y$10$4W/9rxAoX2DqJ5Avjpvlre9q2oIi.yp6fFu1R.9LpS2.gPC/v1Ih6', NULL, 1, 1, 0, '2019-08-08 08:49:14', '2019-11-15 15:13:44', 0),(4, 'Rina', 'y04', '$2y$10$4W/9rxAoX2DqJ5Avjpvlre9q2oIi.yp6fFu1R.9LpS2.gPC/v1Ih6', NULL, 1, 1, 0, '2019-08-08 08:49:14', '2019-11-15 15:13:44', 0);
UNLOCK TABLES;
COMMIT;
