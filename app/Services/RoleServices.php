<?php

namespace App\Services;

use App\Repository\RoleRepository;

class RoleServices
{
    protected $_Role;

    public function __construct(RoleRepository $model)
    {
        $this->_Role = $model;
    }

    /**
     * 取得角色列表
     * @param $request
     * @return array
     */
    public function getRoleList($request)
    {
        $request['page'] = $request['page'] ?? 1;
        $request['pageSize'] = $request['pageSize'] ?? 10;

        $list = $this->_Role->getRoleList($request);
        $rows = $this->_Role->roleListQuery($request)->count();

        return [
            'count' => $rows,
            'list' => $list,
        ];
    }

    /**
     * 取得單筆角色資料
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function getRole($id)
    {
        $isExists = $this->_Role->checkRoleExists($id);

        if ($isExists === 0) {

            throw new \Exception('無法取得單筆角色資料', 404);
        }

        $data = $this->_Role->getRole($id);

        return $data;
    }

    /**
     * 建立角色
     * @param $request
     */
    public function createRole($request)
    {
        $this->_Role->createRole($request);
    }

    /**
     * 更新角色
     * @param $id
     * @param $request
     * @throws \Exception
     */
    public function updateRole($id, $request)
    {
        $isExists = $this->_Role->checkRoleExists($id);

        if ($isExists === 0) {

            throw new \Exception('無法更新 找不到Id', 404);
        }

        $this->_Role->updateRole($id, $request);
    }

    /**
     * 刪除角色
     * @param $id
     * @throws \Exception
     */
    public function deleteRole($id)
    {
        $isExists = $this->_Role->checkRoleExists($id);

        if ($isExists === 0) {

            throw new \Exception('無法刪除 找不到Id', 404);
        }

        $this->_Role->deleteRole($id);
    }
}
