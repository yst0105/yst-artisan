<?php

namespace App\Services;

use App\Repository\UserRoleRepository;

class UserRoleServices
{
    protected $_UserRole;

    public function __construct(UserRoleRepository $model)
    {
     	$this->_UserRole = $model;
    }

    /**
     * 取得使用者-角色列表
     * @param $request
     * @return array
     */
    public function getUserRoleList($request)
    {
        $request['page'] = $request['page'] ?? 1;
        $request['pageSize'] = $request['pageSize'] ?? 10;

        $list = $this->_UserRole->getUserRoleList($request);
        $rows = $this->_UserRole->userRoleListQuery($request)->count();

        return [
            'count' => $rows,
            'list' => $list,
        ];
    }

    /**
     * 取得單筆 使用者-角色
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function getUserRole($id)
    {
        $isExists = $this->_UserRole->checkUserRoleExists($id);

        if($isExists === 0 ){

            throw new \Exception('無法取得 找不到Id', 404);
        }

        $data = $this->_UserRole->getUserRole($id);

        return $data;
    }

    /**
     * 建立 使用-角色
     * @param $request
     */
    public function createUserRole($request)
    {
        $this->_UserRole->createUserRole($request);
    }

    /**
     * 更新 使用-角色
     * @param $id
     * @param $request
     * @throws \Exception
     */
    public function updateUserRole($id, $request)
    {
        $isExists = $this->_UserRole->checkUserRoleExists($id);

        if($isExists === 0 ){

            throw new \Exception('無法更新 找不到Id', 404);
        }

        $this->_UserRole->updateUserRole($id, $request);
    }

    /**
     * 刪除 使用-角色
     * @param $id
     * @throws \Exception
     */
    public function deleteUserRole($id)
    {
        $isExists = $this->_UserRole->checkUserRoleExists($id);

        if($isExists === 0 ){

            throw new \Exception('無法刪除 找不到Id', 404);
        }

        $this->_UserRole->deleteUserRole($id);
    }
}
