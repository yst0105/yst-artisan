<?php

namespace App\Services;

use App\Repository\MenuRepository;

class MenuServices
{
    protected $_Menu;

    public function __construct(MenuRepository $model)
    {
        $this->_Menu = $model;
    }

    /**
     * 取得左側選單
     * @return array
     */
    public function getSideMenu()
    {
        $menuList = $this->_Menu->getSideMenu();
        $menu = $this->sortMenuRecursive($menuList);
        return $menu;
    }

    /**
     * 取得選單列表
     * @param $request
     * @return array
     */
    public function getMenuList($request)
    {
        $request['page'] = $request['page'] ?? 1;
        $request['pageSize'] = $request['pageSize'] ?? 10;

        $list = $this->_Menu->getMenuList($request);
        $rows = 100;

        return [
            'count' => $rows,
            'list' => $list,
        ];
    }

    /**
     * 取得單筆選單
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function getMenu($id)
    {
        $isExists = $this->_Menu->checkMenuExists($id);

        if ($isExists === 0) {

            throw new \Exception('無法取得 找不到Id', 404);
        }

        $data = $this->_Menu->getMenu($id);

        return $data;
    }

    /**
     * 建立選單
     * @param $request
     */
    public function createMenu($request)
    {
        $this->_Menu->createMenu($request);
    }

    /**
     * 修改選單
     * @param $id
     * @param $request
     * @throws \Exception
     */
    public function updateMenu($id, $request)
    {
        $isExists = $this->_Menu->checkMenuExists($id);

        if ($isExists === 0) {

            throw new \Exception('無法更新 找不到Id', 404);
        }

        $this->_Menu->updateMenu($id, $request);
    }

    /**
     * 刪除選單
     * @param $id
     * @throws \Exception
     */
    public function deleteMenu($id)
    {
        $isExists = $this->_Menu->checkMenuExists($id);

        if ($isExists === 0) {

            throw new \Exception('無法刪除 找不到Id', 404);
        }

        $this->_Menu->deleteMenu($id);
    }

    /**
     * 取得選單目錄選項
     * @return mixed
     */
    public function getMenuCatalogOption()
    {
        return $this->_Menu->prepareMenuQuery()->where('idParent', 0)->get();
    }

    /**
     * 取得所有選單列表
     * @return bool
     */
    public function getAllMenuList()
    {
        return $this->_Menu->getAllMenuList();
    }

    /**
     * 遞迴計算
     * @param $menus
     * @param int $pid
     * @return array
     */
    public function sortMenuRecursive($menus, $pid = 0)
    {
        $menu = [];
        foreach ($menus as $key => $value) {
            if ($value->idParent == $pid) {
                $value->isEnabled = (boolean)$value->isEnabled;
                $menu[$key] = $value;
                $menu[$key]->childrens = $this->sortMenuRecursive($menus, $value->id);
            }
            if (isset($menu[$key]->childrens)) {
                array_multisort($menu[$key]->childrens, SORT_ASC);
            }
        }
        return $menu;
    }
}
