<?php

namespace App\Services;

use App\User;

class AuthServices
{
    /**
     * 登入驗證
     * @param $credentials ['account', 'password']
     * @return array
     * @throws \Exception
     */
    public function login($credentials)
    {
        $token = auth('users')->attempt($credentials);

        if (!$token) {
            throw new \Exception('認證失敗', 401);
        }

        $update = User::where('id', auth('users')->user()['id'])->update(['token' => $token]);

        if ($update === false) {
            throw new \Exception('認證更新失敗、請洽聯絡人員', 404);
        }

        return $this->respondWithToken($token);
    }

    /**
     * 回傳格式
     * @param $token
     * @return array
     */
    protected function respondWithToken($token)
    {
        return [
            'id' => auth('users')->user()['id'],
            'name' => auth('users')->user()['name'],
            'account' => auth('users')->user()['account'],
            'accessToken' => $token,
        ];
    }
}
