<?php

namespace App\Services;

use App\Repository\PermissionRepository;
use App\User;

class PermissionServices
{
    protected $_per;
    protected $_menu;

    public function __construct(PermissionRepository $per, MenuServices $menu)
    {
        $this->_per = $per;
        $this->_menu = $menu;
    }

    /**
     * 取得左側選單權限
     * @param $sideMenu
     * @param $jwt
     * @return mixed
     */
    public function getSidePermission($sideMenu, $jwt)
    {
        //取得自己的角色
        $hasRoles = User::find($jwt->get('id'))->hasRoles->pluck('idRoles')->toArray();

        //取得自己所有的權限
        $userPermission = $this->_per->getSidePermissions($hasRoles);

        //合併組成自己所擁有的權限(選單代碼)

        $visible = collect($userPermission)->map(function ($item) {

            return array_keys(json_decode($item->permissions, true));

        })->flatten()->unique()->flip()->toArray();

        //過濾
        $list = collect($sideMenu)->filter(function ($item) use ($visible) {

            if (array_key_exists($item->code, $visible)) {

                return $item;
            }
        })->values()->toArray();

        return $list;
    }

}
