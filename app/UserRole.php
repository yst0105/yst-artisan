<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    //
    protected $table = 'tb_role_users';
    public $timestamps = false;

    public function userRoles()
    {
        return $this->belongsTo('App\Role', 'idRoles', 'id');
    }
}
