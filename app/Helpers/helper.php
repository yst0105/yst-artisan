<?php
/**
 * Created by PhpStorm.
 * User: 20181201
 * Date: 2019/11/8
 * Time: 下午 01:55
 */


use App\User;
use Illuminate\Http\Response;

if (!function_exists('test')) {
    function test()
    {
        return 'test';
    }
}
if (!function_exists('successResponse')) {
    /**
     * @param null $data
     * @param null $totalSize
     * @return \Illuminate\Http\JsonResponse|Response
     */
    function successResponse($data = null, $totalSize = null)
    {
        $response = new Response();

        //有筆數的參數就回傳 list清單
        $reps = ($data !== null && $totalSize !== null) ? ['totalSize' => $totalSize, 'list' => $data] : $data;
        $httpMethod = app('request')->route()->methods()[0];

        switch ($httpMethod) {
            case "GET":
                return response()->json($reps, 200, [], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
                break;
            case "POST":
                return $response->setStatusCode(201);
                break;
            case "PUT":
            case "PATCH":
            case "DELETE":
                return $response->setStatusCode(204);
                break;
        }
        return $response;
    }
}


if (!function_exists('errorResponse')) {

    /**
     * 錯誤回傳
     * 紀錄log
     * @param int $code 預設400
     * @param $trace
     * @return Response
     */
    function errorResponse($code, $trace)
    {
        $code = ($code == 0 || $code == null) ? 400 : $code;
        $response = new Response();
        logger($trace);

        return ($code === 400 || $code === 404) ?
            response()->json($trace->getMessage(), $code, [], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES) :
            $response->setStatusCode($code);
    }
}

if (!function_exists('memberMap')) {
    /**
     * 成員名稱對照
     * @param $memberId
     * @return string
     */
    function memberMap($memberId)
    {
        $userMap = User::all()->pluck('name', 'id')->toArray();
        return $userMap[$memberId] ?? "未知";
    }
}