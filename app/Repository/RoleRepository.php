<?php

namespace App\Repository;

use DB;
use Illuminate\Database\QueryException;

class RoleRepository
{

    private $_table;
    private $_query;

    public function __construct()
    {
        $this->_table = 'tb_roles';
        $this->_query = $this->prepareRoleQuery();
    }

    public function prepareRoleQuery()
    {
        return DB::table($this->_table);
    }

    /**
     * 列表SQL
     * @param $request
     * @return mixed
     */
    public function roleListQuery($request)
    {
        return $this->_query
            ->where('isDeleted', 0)
            ->where(function ($query) use ($request) {
                if (isset($request['isEnabled']) && $request['isEnabled'] === "true") {
                    $query->where('isEnabled', 1);
                } elseif (isset($request['isEnabled']) && $request['isEnabled'] === "false") {
                    $query->where('isEnabled', 0);
                }
            });
    }

    /**
     * 取得角色列表
     * @param $request
     * @return bool
     */
    public function getRoleList($request)
    {
        try {
            return $this->roleListQuery($request)
                ->take($request['pageSize'])
                ->skip($request['page'] > 1 ? ($request['page'] - 1) * $request['pageSize'] : 0)
                ->orderBy('createdOn', 'DESC')
                ->get();
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /**
     * 取得單筆角色
     * @param $id
     * @return bool
     */
    public function getRole($id)
    {
        try {
            return $this->_query->where('id', $id)->first();
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /**
     * 建立角色
     * @param $request
     * @return bool
     */
    public function createRole($request)
    {
        try {
            return $this->_query->insert($request);
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /**
     * 更新角色
     * @param $id
     * @param $request
     * @return bool
     */
    public function updateRole($id, $request)
    {
        try {
            return $this->_query->where('id', $id)->update($request);
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /**
     * 刪除角色
     * @param $id
     * @return bool
     */
    public function deleteRole($id)
    {
        try {
            return $this->_query->where('id', $id)->update(['isDeleted' => 1]);
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /**
     * 確認角色是否存在
     * @param $id
     * @return bool
     */
    public function checkRoleExists($id)
    {
        try {
            return $this->_query->where('id', $id)->count();
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }
}