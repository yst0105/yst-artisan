<?php

namespace App\Repository;

use DB;
use Illuminate\Database\QueryException;

class MenuRepository
{

    private $_table;
    private $_query;

    public function __construct()
    {
        $this->_table = 'tb_menus';
        $this->_query = $this->prepareMenuQuery();
    }

    public function prepareMenuQuery()
    {
        return DB::table($this->_table)->where('isDeleted', 0);
    }

    /**
     * 取得左側選單
     * @return bool
     */
    public function getSideMenu()
    {
        try {
            return $this->_query
                ->select('id', 'name', 'code', 'isEnabled', 'idParent', 'icon', 'route')
                ->where('isEnabled', 1)
                ->orderBy('id', 'ASC')
                ->get();
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /**
     * 取得選單列表(分頁)
     * @param $request
     * @return bool
     */
    public function getMenuList($request)
    {
        try {
            return $this->_query
                ->take($request['pageSize'])
                ->skip($request['page'] > 1 ? ($request['page'] - 1) * $request['pageSize'] : 0)
                ->get();
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /**
     * 取得單筆選單
     * @param $id
     * @return bool
     */
    public function getMenu($id)
    {
        try {
            return $this->_query->where('id', $id)->first();
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /**
     * 更新選單
     * @param $id
     * @param $request
     * @return bool
     */
    public function updateMenu($id, $request)
    {
        try {
            return $this->_query->where('id', $id)->update($request);
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /**
     * 建立選單
     * @param $request
     * @return bool
     */
    public function createMenu($request)
    {
        try {
            return $this->_query->insert($request);
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /**
     * 刪除選單
     * @param $id
     * @return bool
     */
    public function deleteMenu($id)
    {
        try {
            return $this->_query->where('id', $id)->update(['isDeleted' => 1]);
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /**
     * 確認此選單是否存在
     * @param $id
     * @return bool
     */
    public function checkMenuExists($id)
    {
        try {
            return $this->_query->where('id', $id)->count();
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /**
     * 取得所有選單
     * @return bool
     */
    public function getAllMenuList()
    {
        try {
            return $this->_query
                ->select('id', 'name', 'code')
                ->where('isEnabled', 1)
                ->orderBy('id', 'ASC')
                ->get();
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }
}
