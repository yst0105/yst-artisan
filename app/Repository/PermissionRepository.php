<?php

namespace App\Repository;

use App\UserRole;
use DB;
use Illuminate\Database\QueryException;

class PermissionRepository
{

    private $_table;
    private $_query;

    public function __construct()
    {
        $this->_table = 'tb_role_permissions';
        $this->_query = DB::table($this->_table)->where('isDeleted', 0);
    }

    /**
     * 取得自己/角色的權限
     * @param $idRoles
     * @return bool
     */
    public function getSidePermissions($idRoles)
    {
        try {
            return $this->_query
                ->where('isEnabled', 1)
                ->whereIn('idRoles', $idRoles)
                ->get();
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /**
     * 取得權限列表
     * @return bool
     */
    public function getPermissionsList()
    {
        try {
            return $this->_query->get();
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

}