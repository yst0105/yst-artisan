<?php

namespace App\Repository;

use DB;
use Illuminate\Database\QueryException;

class UserRoleRepository
{

    private $_table;
    private $_query;

    public function __construct()
    {
        $this->_table = 'tb_role_users';
        $this->_query = $this->prepareUserRoleQuery();
    }

    public function prepareUserRoleQuery()
    {
        return DB::table($this->_table);
    }

    public function userRoleListQuery($request)
    {
        return $this->_query
            ->where('isDeleted', 0)
            ->where(function ($query) use ($request) {
                if (isset($request['isEnabled']) && $request['isEnabled'] === "true") {
                    $query->where('isEnabled', 1);
                } elseif (isset($request['isEnabled']) && $request['isEnabled'] === "false") {
                    $query->where('isEnabled', 0);
                }
            });
    }

    public function getUserRoleList($request)
    {
        try {
            return $this->userRoleListQuery($request)
                ->take($request['pageSize'])
                ->skip($request['page'] > 1 ? ($request['page'] - 1) * $request['pageSize'] : 0)
                ->get();
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    public function getUserRole($id)
    {
        try {
            return $this->_query->where('id', $id)->first();
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    public function createUserRole($request)
    {
        try {
            return $this->_query->insert($request);
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    public function updateUserRole($id, $request)
    {
        try {
            return $this->_query->where('id', $id)->update($request);
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    public function deleteUserRole($id)
    {
        try {
            return $this->_query->where('id', $id)->update(['isDeleted' => 1]);
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    public function checkUserRoleExists($id)
    {
        try {
            return $this->_query->where('id', $id)->count();
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }

    /*
    public function editUserRole($id)
    {
        try {
            return $this->_query->table->where('id', $id)->first();
        } catch (QueryException $e) {
            logger($e->getMessage(), $e->getTrace());
            return false;
        }
    }
    */
}
