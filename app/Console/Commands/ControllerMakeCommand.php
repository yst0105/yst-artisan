<?php
/**
 * Created by PhpStorm.
 * User: 20181201
 * Date: 2019/2/22
 * Time: 下午 01:46
 */

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class ControllerMakeCommand extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:ctl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Custom Controller class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Controller';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        $name = explode('/', $this->getNameInput('name'));
        $funName = str_replace('Controller', '', $name[0]);

        if($funName =='Menu'){
            return __DIR__ . '/stubs/ctl_menu.stub';
        }

        return __DIR__ . '/stubs/ctl.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Http\Controllers';
    }

    /**
     * Replace the namespace for the given stub.
     *
     * @param  string $stub
     * @param  string $name
     * @return $this
     */
    protected function replaceNamespace(&$stub, $name)
    {
        $name = explode('/', $this->getNameInput('name'));
        $funName = str_replace('Controller', '', $name[0]);

        $stub = str_replace(
            ['NameServices', 'Tb', 'Fn'],
            [$this->option('NameServices'), strtolower($funName), $funName],
            $stub
        );

        return $this;
    }


    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['NameServices', 'm', InputOption::VALUE_OPTIONAL, 'Injection  Controller.'],
            ['Tb', 'r', InputOption::VALUE_OPTIONAL, 'Injection  Controller.'],
        ];
    }

}
