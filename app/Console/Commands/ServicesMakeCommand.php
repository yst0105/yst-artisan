<?php
/**
 * Created by PhpStorm.
 * User: 20181201
 * Date: 2019/2/22
 * Time: 下午 01:46
 */

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class ServicesMakeCommand extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:services';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new services class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Services';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        $name = explode('/', $this->getNameInput('name'));
        $funName = str_replace('Services', '', $name[0]);

        if($funName =='Menu'){
            return __DIR__ . '/stubs/services_menu.stub';
        }

        return __DIR__ . '/stubs/services.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Services';
    }

    /**
     * Replace the namespace for the given stub.
     *
     * @param  string $stub
     * @param  string $name
     * @return $this
     */
    protected function replaceNamespace(&$stub, $name)
    {

        $name = explode('/', $this->getNameInput('name'));
        $funName = str_replace('Services', '', $name[0]);

        $stub = str_replace(
            ['NameRepository', 'Fn'],
            [$this->setModel(), $funName],
            $stub
        );

        return $this;
    }

    /**
     * set Model
     *
     */
    private function setModel()
    {
        if (!empty($this->option('repository'))) {
            return $this->option('repository') . "Repository";
        } else {
            $name = explode('/', $this->getNameInput('name'));
            $new = str_replace('Services', '', $name[0]);
//            return str_replace('Services"', '', $new . "Repository");
            return $new . "Repository";
        }
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['repository', 'm', InputOption::VALUE_OPTIONAL, 'Injection  Services.'],
            ['Tb', 'r', InputOption::VALUE_OPTIONAL, 'Injection  Services.'],
        ];
    }

}
