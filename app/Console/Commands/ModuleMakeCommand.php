<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Artisan;

class ModuleMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:module';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '[創建][功能模組]';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $module = $this->choice('你要建立的功能模組為?', [
            'Admin',
            'Menu',
            'WhiteIp'
        ], 0);


        if ($this->confirm('要建立的模組: ' . $module)) {

            switch ($module) {
                case 'Menu':
                    Artisan::call('module:Menu');
                    $this->info(Artisan::output());
                    $this->info('請將以下張貼於 api.php' . '
Route::get(\'/sideMenus\', \'MenuController@getSideMenu\');
Route::get(\'/menus\', \'MenuController@getMenuList\');
Route::post(\'/menus\', \'MenuController@createMenu\');
Route::get(\'/menus/{id}\', \'MenuController@getMenu\');
Route::patch(\'/menus/{id}\', \'MenuController@updateMenu\');
Route::delete(\'/menus/{id}\', \'MenuController@deleteMenu\');
                    ');
                    break;
                case 'Admin':
                    $this->info('還沒建立好');
                    break;
            }


            $this->info('成功建立 ' . $module . ' 功能模組');
        }
    }
}
