<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Artisan;

class AllMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '[創建][Controller、Services、Repository]';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $className = $this->ask('你要創建的Controller、Services、Repository名稱為?');

        if ($className && preg_match('/^[A-Za-z0-9]+$/', $className)) {

            $controllerName = $className . 'Controller';
            $servicesName = $className . 'Services';
            $repositoryName = $className . 'Repository';

            $tableName = $this->ask('請輸入 Table name');


            if ($this->confirm('確認創建' . $controllerName . "、" . $servicesName . "、" . $repositoryName)) {

                Artisan::call('make:ctl', ['name' => $controllerName, '--NameServices' => $className]);
                $this->info(Artisan::output());

                Artisan::call('make:services', ['name' => $servicesName, '--repository' => $className]);
                $this->info(Artisan::output());

                Artisan::call('make:repository', ['name' => $repositoryName, '--Tb' => $tableName]);
                $this->info(Artisan::output());

            } else {
                $this->error('沒事發生');
            }
        } else {
            $this->error('您輸入的名稱不符合英數字');
//            exit ;
        }


        /*
        //->多選擇
        if ($this->confirm('您是否建立controller、services、repository')) {
            $choose = $this->choice('如何建立?', ['不使用相同名稱', '使用相同名稱']);

            if ($choose == 0) {

                $controllerName = $this->ask('您要創建的Controller名稱為?');
                if ($controllerName && preg_match('/^[A-Za-z0-9]+$/', $controllerName)) {
                    $controllerName = $controllerName . 'Controller';
                    Artisan::call('make:controller', ['name' => $controllerName]);
                    $this->info(Artisan::output());

                } else {
                    $this->error('不符合英數字的名稱');
                    exit(0);
                }

                $servicesName = $this->ask('您要創建的Services名稱為??');
                if ($servicesName && preg_match('/^[A-Za-z0-9]+$/', $servicesName)) {
                    $servicesName = $servicesName . 'Services';
                    Artisan::call('make:services', ['name' => $servicesName]);
                    $this->info(Artisan::output());
                } else {
                    $this->error('不符合英數字的名稱');
                    exit(0);
                }

                $RepositoryrName = $this->ask('您要創建的Repository名稱為?');
                if ($RepositoryrName && preg_match('/^[A-Za-z0-9]+$/', $RepositoryrName)) {
                    $repositoryName = $RepositoryrName . 'Repository';
                    Artisan::call('make:services', ['name' => $repositoryName]);
                    $this->info(Artisan::output());
                } else {
                    $this->error('不符合英數字的名稱');
                    exit(0);
                }
            } else if ($choose == 1) {
                $className = $this->ask('你要創建的Controller、Services、Repository名稱為?');

                $controllerName = $className . 'Controller';
                $servicesName = $className . 'Services';
                $repositoryName = $className . 'Repository';

                if ($this->confirm('確定創建' . $controllerName . "、" . $servicesName . "、" . $repositoryName)) {
                    Artisan::call('make:controller', ['name' => $controllerName]);
                    $this->info(Artisan::output());

                    Artisan::call('make:services', ['name' => $servicesName]);
                    $this->info(Artisan::output());

                    Artisan::call('make:repository', ['name' => $repositoryName]);
                    $this->info(Artisan::output());
                }
            }
        }
        */
    }
}
