<?php
/**
 * Created by PhpStorm.
 * User: 20181201
 * Date: 2019/2/22
 * Time: 下午 01:46
 */

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class RepositoryMakeCommand extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:repository';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new repository class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Repository';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        $name = explode('/', $this->getNameInput('name'));
        $funName = str_replace('Repository', '', $name[0]);

        if($funName =='Menu'){
            return __DIR__ . '/stubs/repository_menu.stub';
        }
        return __DIR__ . '/stubs/repository.stub';
    }


    /**
     * Replace the namespace for the given stub.
     *
     * @param  string $stub
     * @param  string $name
     * @return $this
     */
    protected function replaceNamespace(&$stub, $name)
    {
        $name = explode('/', $this->getNameInput('name'));
        $modelName = str_replace('Repository', '', $name[0]);

        $stub = str_replace(
            ['Tb', 'Fn'],
            [
                $this->option('Tb'),
                $modelName
            ],
            $stub
        );

        return $this;

    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Repository';
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['Tb', 'm', InputOption::VALUE_OPTIONAL, 'Injection  model.'],
            ['Fn', 'r', InputOption::VALUE_OPTIONAL, 'Injection  model.']
        ];
    }
}