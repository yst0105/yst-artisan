<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class MenuMakeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:Menu';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '建立選單模組';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        if (!Schema::hasTable('menus')) {
            Schema::create('menus', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 20)->comment('選單名稱');
                $table->string('code', 20)->comment('選單代碼');
                $table->tinyInteger('idParent')->default(0)->comment('上層選單');
                $table->string('icon', 50)->comment('選單icon');
                $table->string('route', 30)->comment('路由');
                $table->integer('sort')->default(1)->comment('排序');
                $table->tinyInteger('isEnabled')->index()->default(1)->comment('啟用狀態');
                $table->integer('createdBy')->default(0)->comment('建立者');
                $table->integer('updatedBy')->default(0)->comment('修改者');
                $table->dateTime('createdOn')->default(DB::raw('CURRENT_TIMESTAMP'))->comment('建立日期');
                $table->dateTime('updatedOn')->index()->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'))->comment('最後更新時間');
                $table->tinyInteger('isDeleted')->default(0)->index()->comment('軟刪除 (0:未刪除|1:已刪除)');
                // $table->timestamps();

                // 索引
                $table->unique('code');
            });

            $menu = [
                [
                    'name' => '帳號管理',
                    'code' => 'accounts',
                    'idParent' => 0,
                    'icon' => 'el-icon-setting',
                    'route' => 'accounts'
                ],
                [
                    'name' => '選單管理',
                    'code' => 'menus',
                    'idParent' => 0,
                    'icon' => 'el-icon-menu',
                    'route' => 'menus',
                ],
                [
                    'name' => '權限管理',
                    'code' => 'permissions',
                    'idParent' => 0,
                    'icon' => 'el-icon-sold-out',
                    'route' => 'permissions'
                ],
                [
                    'name' => '通知管理',
                    'code' => 'notifies',
                    'idParent' => 0,
                    'icon' => 'el-icon-bell',
                    'route' => 'notifies'
                ],
                [
                    'name' => '庫存管理',
                    'code' => 'materials',
                    'idParent' => 0,
                    'icon' => 'el-icon-document',
                    'route' => 'materials'
                ],
                [
                    'name' => 'Telegram',
                    'code' => 'telegram',
                    'idParent' => 4,
                    'icon' => '',
                    'route' => 'telegram'
                ],
                [
                    'name' => 'Line',
                    'code' => 'line',
                    'idParent' => 4,
                    'icon' => '',
                    'route' => 'line'
                ],
                [
                    'name' => '角色列表',
                    'code' => 'roles',
                    'idParent' => 1,
                    'icon' => '',
                    'route' => 'roles'
                ],
                [
                    'name' => '使用者列表',
                    'code' => 'users',
                    'idParent' => 1,
                    'icon' => '',
                    'route' => 'users'
                ]
            ];
            DB::table('menus')->insert($menu);


            Artisan::call('make:ctl', ['name' => 'MenuController', '--NameServices' => 'Menu']);
            $this->info(Artisan::output());

            Artisan::call('make:services', ['name' => 'MenuServices', '--repository' => 'Menu']);
            $this->info(Artisan::output());

            Artisan::call('make:repository', ['name' => 'MenuRepository', '--Tb' => 'menus']);
            $this->info(Artisan::output());
        }
    }
}
