<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'tb_roles';
    public $timestamps = false;

    public function userRoles()
    {
        return $this->hasMany('App\UserRole', 'idRoles', 'id');
    }
}
