<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserRole\DetailResource;
use App\Http\Resources\UserRole\ListResource;
use App\Services\UserRoleServices;
use Illuminate\Http\Request;

class UserRoleController extends Controller
{
    protected $_userrole;

    public function __construct(UserRoleServices $UserRole)
    {
        $this->_userrole = $UserRole;
    }

    /**
     * 取得使用者-角色列表
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function getUserRoleList(Request $request)
    {
        try {
            $list = $this->_userrole->getuserroleList(
                $request->only([
                    'isEnabled',
                    'page',
                    'pageSize',
                    'sort',
                    'sortName'
                ]));
            return successResponse(ListResource::collection($list['list']), $list['count']);
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }

    /**
     * 取得單筆 使用者-角色
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function getUserRole($id)
    {
        try {
            $data = $this->_userrole->getuserrole($id);
            return successResponse(new DetailResource($data));
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }

    /**
     * 建立 使用者-角色
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function createUserRole(Request $request)
    {
        try {
            $this->_userrole->createuserrole($request->only([
                'idUsers',
                'idRoles',
                'isEnabled',
            ]));
            return successResponse();
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }

    /**
     * 更新 使用者-角色
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function updateUserRole($id, Request $request)
    {
        try {
            $this->_userrole->updateUserRole($id, $request->only([
                'idUsers',
                'idRoles',
                'isEnabled'
            ]));
            return successResponse();
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }

    /**
     * 刪除 使用者-角色
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function deleteUserRole($id)
    {
        try {
            $this->_userrole->deleteUserRole($id);
            return successResponse();
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }
}
