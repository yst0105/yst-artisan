<?php

namespace App\Http\Controllers;

use App\Http\Resources\Role\DetailResource;
use App\Http\Resources\Role\ListResource;
use App\Services\RoleServices;
use Illuminate\Http\Request;

use Tymon\JWTAuth\JWTAuth;

class RoleController extends Controller
{
    protected $_role;
    protected $_jwt;

    public function __construct(RoleServices $Role, JWTAuth $jwt)
    {
        $this->_jwt = $jwt->parseToken()->getPayload();
        $this->_role = $Role;
    }

    /**
     * 取得角色清單
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function getRoleList(Request $request)
    {
        try {
            $list = $this->_role->getroleList(
                $request->only([
                    'isEnabled',
                    'page',
                    'pageSize',
                    'sort',
                    'sortName'
                ]));
            return successResponse(ListResource::collection($list['list']), $list['count']);
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }

    /**
     * 取得單筆角色
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function getRole($id)
    {
        try {
            $data = $this->_role->getrole($id);
            return successResponse(new DetailResource($data));
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }

    /**
     * 建立角色
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function createRole(Request $request)
    {
        try {
            $this->_role->createrole($request->only(['name', 'remark', 'isEnabled']));
            return successResponse();
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }

    /**
     * 更新角色
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function updateRole($id, Request $request)
    {
        try {
            $this->_role->updaterole($id, $request->only(['name', 'remark', 'isEnabled']));
            return successResponse();
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }

    /**
     * 刪除角色
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function deleteRole($id)
    {
        try {
            $this->_role->deleteRole($id);
            return successResponse();
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }
}
