<?php

namespace App\Http\Controllers;

use App\Services\AuthServices;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    private $_auth;

    public function __construct(AuthServices $auth)
    {
        $this->_auth = $auth;
    }

    /**
     * 登入驗證
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        try {
            $auth = $this->_auth->login($request->only(['account', 'password']));
            return response()->json($auth, 200);
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }

    }
}
