<?php

namespace App\Http\Controllers;

use App\Http\Resources\Menu\DetailResource;
use App\Http\Resources\Menu\OptionResource;
use App\Services\MenuServices;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    protected $_menu;

    public function __construct(MenuServices $Menu)
    {
        $this->_menu = $Menu;
    }

    public function getSideMenu()
    {
        try {
            $side = $this->_menu->getSidemenu();
            return successResponse($side, count($side));
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }

    public function getMenuList(Request $request)
    {
        try {
            $list = $this->_menu->getmenuList(
                $request->only([
                    'page',
                    'pageSize',
                    'sort',
                    'sortName'
                ]));
            return successResponse($list['list'], $list['count']);
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }

    public function getMenu($id)
    {
        try {
            $data = $this->_menu->getmenu($id);
            return successResponse(new DetailResource($data));
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }

    public function createMenu(Request $request)
    {
        try {
            $this->_menu->createmenu($request->only([
                'name',
                'code',
                'idParent',
                'icon',
                'route',
                'isEnabled'
            ]));
            return successResponse();
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }

    public function updateMenu($id, Request $request)
    {
        try {
            $this->_menu->updatemenu($id, $request->only([
                'name',
                'code',
                'idParent',
                'icon',
                'route',
                'isEnabled'
            ]));
            return successResponse();
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }

    public function deleteMenu($id)
    {
        try {
            $this->_menu->deleteMenu($id);
            return successResponse();
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }

    public function getMenuCatalogOption()
    {
        try {
            $option = $this->_menu->getMenuCatalogOption();
            return successResponse(OptionResource::collection($option), $option->count());
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }
}
