<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use App\Services\MenuServices;
use App\Services\PermissionServices;
use App\UserRole;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;

class PermissionController extends Controller
{
    protected $_per;
    protected $_menu;
    protected $_jwt;

    public function __construct(
        PermissionServices $per,
        MenuServices $menu,
        JWTAuth $jwt
    )
    {
        $this->_per = $per;
        $this->_menu = $menu;
        $this->_jwt = $jwt->parseToken()->getPayload();
    }

    /**
     * 取得左側權限選單
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function getSidePermission()
    {
        try {
            $sideMenu = $this->_menu->getSideMenu();
            $list = $this->_per->getSidePermission($sideMenu, $this->_jwt);
            return successResponse($list, count($list));
        } catch (\Exception $e) {
            return errorResponse($e->getCode(), $e);
        }
    }

    /**
     * 取得權限總表
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
//    public function getPermissionList()
//    {
//        try {
//            $list = $this->_per->getPermissionList();
//            return successResponse($list, count($list));
//        } catch (\Exception $e) {
//            return errorResponse($e->getCode(), $e);
//        }
//    }
}
