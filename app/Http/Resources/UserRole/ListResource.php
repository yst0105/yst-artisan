<?php

namespace App\Http\Resources\UserRole;
use App\UserRole;
use Illuminate\Http\Resources\Json\JsonResource;

class ListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'idUsers' => $this->idUsers,
            'idRoles' => $this->idRoles,
            'nameUsers' => memberMap($this->idUsers),
            'nameRoles' => UserRole::find($this->idRoles)->userRoles->name,
            'createdBy' => memberMap($this->createdBy),
            'updatedOn' => substr($this->updatedOn,0,16)
        ];
    }
}
