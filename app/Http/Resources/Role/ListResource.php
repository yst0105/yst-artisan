<?php

namespace App\Http\Resources\Role;

use Illuminate\Http\Resources\Json\JsonResource;

class ListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'remark' => $this->remark,
            'isEnabled' => (boolean)$this->isEnabled ? '已啟用' : '停用中',
            'createdBy' => memberMap($this->createdBy),
            'createdOn' => substr($this->createdOn, 0, 16),
        ];
    }
}
